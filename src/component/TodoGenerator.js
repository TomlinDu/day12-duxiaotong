import {useState} from 'react';
import { UseSelector,useDispatch } from 'react-redux';
import {addTodo} from './todoListSlice';

const TodoGenerator= ()=>{
    const dispatch= useDispatch();
    const [inputValue,setInputValue]=useState("");
    const handleAddTodo=()=>{
        dispatch(addTodo({
            id:Date.now(),
            text:inputValue,
            done:false,
        }));
    }
    return(
        <div className='inputMessage'>
            <input value={inputValue} onChange={(event)=>setInputValue(event.target.value)}></input>
            <button onClick={handleAddTodo}>add</button>
        </div>
    )
}
export default TodoGenerator;
