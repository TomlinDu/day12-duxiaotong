import Header from './Header';
import TodoGenerator from './TodoGenerator';
import TodoGroup from './TodoGroup';
import { useState } from 'react';
function TodoList(){
  
    return(
        <div className='todoList'>
            <Header />
            <TodoGroup />
            <TodoGenerator />
        </div>
    )
}
export default TodoList;
