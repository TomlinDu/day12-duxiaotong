import Item from "./Item";
import './TodoGroup.css'
import { useSelector } from 'react-redux';

function TodoGroup(){
    const todoList = useSelector(state => state.todo.todoList);
    return(
        <div className='todoGroup'>
            {todoList.map((todo)=>{
              return  (<Item key={todo.id} todo={todo}/>);
            })}
        </div>
    )
}
export default TodoGroup;
